# SMPE Lessons :
### 1. 28/09/22 [ JMV, AL ] Introduction to the scientific method and computer science epistemology | Presentation of the lecture
#### Homeworks

+ [x] Read Popper’s text
+ [x] Indicate your name on the Pad. You will use to collaborate and fill in all the information you can.
+ [x] Register on the Mattermost through this invitation link. This is the preferred communication mode.
+ [x] Set up a public github or gitlab project for this lecture. You will take notes on this lecture and turn your homework and computational documents in this project.
+ [x] Register to the MOOC on Reproducible Research
+ [x] Follow modules 1 + 2 of the MOOC with as much exercises as possible (except the last one of module2, on Challenger; watching interviews is optional)
+ [x] Set up a computational document system (e.g., Rstudio or Jupyter on your laptop or through the UGA JupyterHub).
+ [x] Report the URL of your git project, your mattermost ID on the Pad.

### 2. 05/10/23 [ JMV, AL ] Visualization and Exploratory Data Analysis | Descriptive statistics; Correlation, causality, and spurious correlation
#### Homeworks

+ [x] Start learning R by reading this R crash course for computer scientists (Rmd sources).
+ [x] Criticize every figure of Jean-Marc’s slides by:
    +1 Applying the checklist for good graphics;
    +2 Proposing a better representation (hand-drawing is fine) that passes the checklist.
+ [x] Report this work for at least 3 figures on you github/gitlab project.
+ [x] MOOC: Complete exercise 5 of module 2 (Challenger). Write a short text explaining what is good and wrong about this document (you may want to provide an updated version of the notebook) and upload on your github/gitlab space.

### 3. 19/10/23 [ AL ] Processing data with the Tidyverse, Data curation, beautiful viz with ggplot
#### Homeworks

+ [x] Use good naming and organization conventions in your repos.
+ [x] Complete the Challenger exercise if you haven’t done so.
+ [ ] Engage in module 3 of the MOOC and choose a topic for the peer evaluation (final task of module 3).
+ [ ] Here is an opportunity to practice your data curation/analysisskills by producing the First name/last name analysis in a computational document. I encourage you to use dplyr and ggplot for this analysis.
